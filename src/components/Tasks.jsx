import React from "react";
import Task from "./Task";


const Tasks = ({tasks,Delete,reminder}) => {

  return (
    <div className="">
      {tasks.map((task) => (
      <Task key={task.id} task={task} Delete={Delete} reminder={reminder}  />
      ))}
    </div>
  );
};

export default Tasks;
