import React from "react";
import PropTypes from "prop-types";

const Button = ({ bgcolor, text, color ,onAdd}) => {
 
  return (
    <div>
      <button
        className="btn"
        style={{ backgroundColor: bgcolor, color: color }}
        onClick={onAdd}
      >
        {text}
      </button>
    </div>
  );
};

Button.defaukltProps = {
  color: "steelblue",
};

Button.propTypes = {
  text: PropTypes.string,
  color: PropTypes.string,
  Add:PropTypes.func
};

export default Button;
