import React,{useState} from "react";


const Addtask = ({Add}) => {

    const [text,settext]=useState('');
    const [day,setday]= useState('');
    const [reminder,setreminder]=useState(false);

    const onSubmit=(e)=>{
        e.preventDefault();

        if(!text)
        {
            alert('Please enter a task');
            return;
        }

        Add({text,day,reminder})
        
        setday('');
        settext('');
        setreminder(false);
    }
  return (
    <form className="add-form" onSubmit={onSubmit}>
      <div className="form-control">
        <label>Task</label>
        <input type="text" placeholder="Add Task" value={text} onChange={(e)=>settext(e.target.value)} />

      </div>
      <div className="form-control">
        <label>Date</label>
        <input type="text" placeholder="What's the Date !?" value={day}  onChange={(e)=>setday(e.target.value)}/>

      </div>
      <div className="form-control form-control-check">
        <label>Reminder</label>
        <input type="checkbox" value={reminder} checked={reminder} onChange={(e)=>setreminder(e.currentTarget.checked)}  />

      </div>
      <input type="submit"  value='Save Task' className="btn btn-block"/>
      
    </form>
  );
};

export default Addtask;
