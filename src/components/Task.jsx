import React from 'react'
import {FaTimes} from 'react-icons/fa'

const Task = ({task,Delete,reminder}) => {
  return (
    <div className={`task ${task.reminder?'reminder':''}`} onDoubleClick={()=>reminder(task.id)}>
      <h3>{task.title}<FaTimes onClick={()=>Delete(task.id)} style={{color:'red',cursor:"pointer"}}/></h3>
      <p>{task.date}</p> 
    </div>
  )
}

export default Task
