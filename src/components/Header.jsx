import React from 'react'
import PropTypes from 'prop-types'
import Button from './Button'


function Header({ title, onAdd, showtask }) {

  return (
    <header className='header'>
      <h1>{title}</h1>
      <Button bgcolor={showtask ? 'Red' : 'green'} color='white' text={showtask ? 'Close' : "Add"} onAdd={onAdd} />

    </header>
  )
}
// Incase PArentcomponent forgets to send props this will set automatically
Header.defaultProps = {
  title: 'My Tasks'
}

Header.propTypes = {
  title: PropTypes.string.isRequired
}

export default Header
