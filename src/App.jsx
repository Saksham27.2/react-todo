import React, { useState } from "react";
import "./App.css";
import Header from "./components/Header";
import Tasks from "./components/Tasks";
import Addtask from "./components/Addtask";


const App = () => {
  const [showAddtask, setshowaddtask] = useState(false);
  const [tasks, settasks] = useState([
    { id: 1, title: "Take out trash", date: "2 April 23", reminder: false },
    { id: 2, title: "Dinner", date: "1 April 23", reminder: true },
    { id: 3, title: "Workout", date: "1 April 23", reminder: false },
    { id: 4, title: "Walk", date: "2 April 23", reminder: true },
  ]);
  // Adding the Task
  const add = (task) => {
    const id = Math.floor(Math.random() * 10000) + 1;

    const newtask = { id, title: task.text, day: task.date, reminder: task.reminder }
    console.log(newtask);
    settasks([...tasks, newtask])
    console.log(tasks);
  }



  // Deleting task
  const Delete = (id) => {
    settasks(tasks.filter((task) => task.id !== id));
    console.log(`id  is Deleted`);
  };

  // Toggle Reminder
  const reminder = (id) => {
    settasks(
      tasks.map((task) =>
        task.id === id
          ? {
            ...task,
            reminder: !task.reminder,
          }
          : task
      )
    );
  };
  return (
    <div className="container">
      <Header title={"My Tasks"} onAdd={()=>setshowaddtask(!showAddtask) }showtask={showAddtask}  />

      {
        showAddtask &&  <Addtask Add={add} />
      }
      



      {tasks.length > 0 ? (
        <Tasks tasks={tasks} Delete={Delete} reminder={reminder} />
      ) : (
        "No Tasks To Shown"
      )}
    </div>
  );
};

export default App;
